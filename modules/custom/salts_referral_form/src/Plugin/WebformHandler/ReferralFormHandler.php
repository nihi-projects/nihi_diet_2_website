<?php
namespace Drupal\salts_referral_form\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\webformSubmissionInterface;
use GuzzleHttp\Cookie\CookieJar;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "salts_referral_form_form_handler",
 *   label = @Translation("SALTS Referral Form handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Prepare submission data for Koda API."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class ReferralFormHandler extends WebformHandlerBase {

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
        
        // Get an array of the values from the submission.
        /*
            $webform_submission->setSticky(!$webform_submission->isSticky())->save();
            $sid = $webform_submission->id();        
        */

        $config = \Drupal::config('salts_referral_form.adminsettings');

	    $database = \Drupal::database();                        
	    $most_recent_submission = $database->query("SELECT AUTO_INCREMENT AS auto_increment FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $config->get('database') . "' AND TABLE_NAME = 'webform_submission';")->fetch();
        $sid = ($most_recent_submission) ? $most_recent_submission->auto_increment : 1;

        $submission = $webform_submission->getData();

        // Save submission to backend table.
        // $this->saveSubmission($sid, $submission);

        // Convert submission to JSON.        
        $json_array = $this->convertToJSON($sid, $submission);

        // Post JSON to Koda API.
        $response_array = $this->postSubmissionToKoda($json_array);

        // For debugging
        $message = '';
        foreach($response_array as $key => $value) {
            $message .= '<h4>' . $key . '</h4>';
            $message .= $value;
        }

        \Drupal::logger('salts_referral_form')->error($message);
        
        return true;

    }

    /**
     * Saves submission to referral_form_submission table.
     * 
     * @param $sid
     *  The study code of participant.
     *  
     * @param array $submission
     *  The submission data.
     */
    protected function saveSubmission($sid, $submission) {

        $database = \Drupal::database();

        $fields = [
            'study_code' => $sid,
            'field_date_completed' => strtotime(date()),
            'field_first_name' => $submission['field_first_name'],
            'field_last_name' => $submission['field_last_name'],
            'field_mobile_number' => $submission['field_mobile_number'],
            'field_email_address' => $submission['field_email_address'],
            'field_height' => $submission['field_height'],
            'field_weight' => $submission['field_weight'],
            'field_systolic_blood_pressure_measure_first' => $submission['field_systolic_blood_pressure_measure_first'],
            'field_diastolic_blood_pressure_measure_first' => $submission['field_diastolic_blood_pressure_measure_first'],
            'field_systolic_blood_pressure_measure_second' => $submission['field_systolic_blood_pressure_measure_second'],
            'field_diastolic_blood_pressure_measure_second' => $submission['field_diastolic_blood_pressure_measure_second'],
            'field_systolic_blood_pressure_measure_third' => $submission['field_systolic_blood_pressure_measure_third'],
            'field_diastolic_blood_pressure_measure_third' => $submission['field_diastolic_blood_pressure_measure_third'],
            'field_has_smartphone' => $submission['field_has_smartphone'],
            'field_participant_consent' => $submission['field_participant_consent'],
            'field_referrer_name' => $submission['field_referrer_name'],
            'field_referrer_organisation' => $submission['field_referrer_organisation'],            
        ];

        $database->insert('referral_form_submission')->fields($fields)->execute();

    }


    /**
     * Converts submission to JSON.
     * 
     * @param $sid
     *  The study code of participant.
     * 
     * @param array $submission
     *  The submission data.
     * 
     * @return array $json_array
     *  The submission in JSON format.
     */
    protected function convertToJSON($sid, $submission) {

	    $json_array = [];

        $field_blood_pressure = [];
        if ( strlen($submission['field_systolic_blood_pressure_measure_first']) > 0 && strlen($submission['field_diastolic_blood_pressure_measure_first']) > 0  ) {
            $field_blood_pressure[] = $submission['field_systolic_blood_pressure_measure_first'] . '/' . $submission['field_diastolic_blood_pressure_measure_first'];
        }
        if ( strlen($submission['field_systolic_blood_pressure_measure_second']) > 0 && strlen($submission['field_diastolic_blood_pressure_measure_second']) > 0  ) {
            $field_blood_pressure[] = $submission['field_systolic_blood_pressure_measure_second'] . '/' . $submission['field_diastolic_blood_pressure_measure_second'];
        }
        if ( strlen($submission['field_systolic_blood_pressure_measure_third']) > 0 && strlen($submission['field_diastolic_blood_pressure_measure_third']) > 0  ) {
            $field_blood_pressure[] = $submission['field_systolic_blood_pressure_measure_third'] . '/' . $submission['field_diastolic_blood_pressure_measure_third'];
        }

        $form_data = new \stdClass();
        $form_data->field_date_completed = date("Y-m-d\TH:i:s");
        $form_data->field_first_name = $submission['field_first_name'];
        $form_data->field_last_name = $submission['field_last_name'];
        $form_data->field_mobile_number = $submission['field_mobile_number'];
        $form_data->field_email_address = $submission['field_email_address'];
        $form_data->field_has_smartphone = ($submission['field_has_smartphone'] === 'Yes') ? 1 : 0;
        $form_data->field_height = $submission['field_height'];
        $form_data->field_weight = $submission['field_weight'];
        $form_data->field_blood_pressure = $field_blood_pressure;
        $form_data->field_participant_consent = ($submission['field_participant_consent'] === "Yes") ? 1 : 0;
        $form_data->field_referrer_name = $submission['field_referrer_name'];
	$form_data->field_referrer_organisation = $submission['field_referrer_organisation'];

        $json_object = new \stdClass();
        $json_object->form_type = "referral_form";
        $json_object->form_id = "unique_id_" . $sid;
        $json_object->form_data = $form_data;

        $json_array[] = $json_object;

        return $json_array;

    }


    /**
     * Post submission data to Koda API.
     * 
     * 
     * @param array $json_array
     *  The JSON to post.
     * 
     * @return array $response_array
     *  Array of responses from post in string form.
     * 
     */
    protected function postSubmissionToKoda($json_array) {

        $response_array = [];

        $config = \Drupal::config('salts_referral_form.adminsettings');

        $json_string = json_encode($json_array, JSON_UNESCAPED_SLASHES);

        $jar = new CookieJar();

        try {

            // Login
            
            $response = \Drupal::httpClient()->post(
                $config->get('base_url') . '/user/login?_format=json',
                [
                    'body' => '{"name":"' . $config->get('username') . '","pass":"' . $config->get('password') . '"}',
                    'headers' => [
                        'Content-Type' => 'application/json',                    
                    ],
                    'cookies' => $jar
                ]
            );
            $response_json_login = json_decode($response->getBody()->getContents());
            $response_array['Login Request'] = '<div><pre>POST ' . $config->get('base_url') . '/user/login?_format=json' . '</pre></div>';
            $response_array['Login Response'] = '<div><pre>' . print_r($response_json_login, true) . '</pre></div>';


            // Login Status

            $response = \Drupal::httpClient()->get(
                $config->get('base_url') . '/user/login_status?_format=json',
                [
                    'cookies' => $jar
                ]
            );
            $response_status = $response->getBody()->getContents();
            $response_array['Login Status Request'] = '<div><pre>GET ' . $config->get('base_url') . '/user/login_status?_format=json' . '</pre></div>';
            $response_array['Login Status Response'] = '<div><pre>' . print_r($response_status, true) . '</pre></div>';


            // Session Token

            $response = \Drupal::httpClient()->get(
                $config->get('base_url') . '/rest/session/token',
                [
                    'cookies' => $jar
                ]
            );
            $response_token = $response->getBody()->getContents();
            $response_array['Session Token Request'] = '<div><pre>GET ' . $config->get('base_url') . '/rest/session/token' . '</pre></div>';
            $response_array['Session Token Response'] = '<div><pre>' . print_r($response_token, true) . '</pre></div>';


            // Submit Referral
            
            $response = \Drupal::httpClient()->post(
                $config->get('base_url') . '/api/site/referral-submit-rest',
                [                
                    'body' => $json_string,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'X-CSRF-Token' => $response_json_login->csrf_token,
                    ],
                    'auth' => [ $config->get('username'), $config->get('password') ],
                    'cookies' => $jar
                ]
            );
            $response_json_submit = json_decode($response->getBody()->getContents());
            $response_array['Submit Referral Request'] = '<div><pre>POST ' . $config->get('base_url') . '/api/site/referral-submit-rest' . '</pre></div>';
            $response_array['Submit Referral JSON'] = '<div><pre>' . $json_string . '</pre></div>';
            $response_array['Submit Referral Response'] = '<div><pre>' . print_r($response_json_submit, true) . '</pre></div>';
            

            // Logout
            
            $response = \Drupal::httpClient()->post(
                $config->get('base_url') . '/user/logout?_format=json&token=' . $response_json_login->logout_token,
                [
                    'body' => '{}',
                    'headers' => [
                        'Content-Type' => 'application/json',                    
                    ],
                    'cookies' => $jar
                ]
            );
            $response_json_logout = json_decode($response->getBody()->getContents());
            $response_array['Logout Request'] = '<div><pre>POST ' . $config->get('base_url') . '/user/logout?_format=json&token=' . $response_json_login->logout_token . '</pre></div>';
            $response_array['Logout Response'] = '<div><pre>' . print_r($response_json_logout, true) . '</pre></div>';
        

        }
        catch(\Exception $e) {
            $response_array['Error Encountered'] = '<div><pre>' . $e->getMessage() . '</pre></div>';
        }

        return $response_array;

    }

}   


