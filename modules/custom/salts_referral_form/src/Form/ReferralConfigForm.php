<?php 

/**
 * @file
 * Contains Drupal\salts_referral_form\Form\ReferralConfigForm.
 * 
 */
namespace Drupal\salts_referral_form\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ReferralConfigForm extends ConfigFormBase {

    /**  
     * {@inheritdoc}  
     */  
    protected function getEditableConfigNames() {  
        return [  
            'salts_referral_form.adminsettings',  
        ];  
    }  

    /**  
     * {@inheritdoc}  
     */  
    public function getFormId() {  
        return 'salts_referral_form_form';  
    }  

    /**  
     * {@inheritdoc}  
     */  
    public function buildForm(array $form, FormStateInterface $form_state) {  
        $config = $this->config('salts_referral_form.adminsettings');  

        $form['database'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Database name'),  
            '#description' => $this->t('Database name'),  
            '#default_value' => $config->get('database'),  
        ];

        $form['username'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Username'),  
            '#description' => $this->t('Username'),  
            '#default_value' => $config->get('username'),  
        ];
        
        $form['password'] = [  
            '#type' => 'password',  
            '#title' => $this->t('Password'),  
            '#description' => $this->t('Password'),  
            '#default_value' => $config->get('password'),  
        ];

        $form['base_url'] = [
            '#type' => 'textfield',  
            '#title' => $this->t('Base URL of Koda endpoint'),  
            '#description' => $this->t('Base URL of Koda endpoint'),  
            '#default_value' => $config->get('base_url'),
        ];

        return parent::buildForm($form, $form_state);  
    }
    
    /**  
     * {@inheritdoc}  
     */  
    public function submitForm(array &$form, FormStateInterface $form_state) {  
        parent::submitForm($form, $form_state);  

        $this->config('salts_referral_form.adminsettings')  
        ->set('database', $form_state->getValue('database'))
        ->set('username', $form_state->getValue('username'))
        ->set('password', $form_state->getValue('password'))  
        ->set('base_url', $form_state->getValue('base_url'))        
        ->save();  
        
    }  




}

