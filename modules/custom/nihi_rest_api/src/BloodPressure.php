<?php

namespace Drupal\nihi_rest_api;
class BloodPressure { 
        protected $id;
        protected $transaction_guid;
        protected $transaction_timestamp;
        protected $upload_id;
        protected $device_name;
        protected $serial_number;
        protected $manufacturer_number;
        protected $model_number;
        protected $battery_level;
        protected $user_number;
        protected $transmission_timestamp;
        protected $reading_timestamp;
        protected $reading_source;
        protected $bp_systolic_value;
        protected $bp_systolic_unit;
        protected $bp_diastolic_value;
        protected $bp_diastolic_unit;
        protected $bp_heartrate_value;
        protected $bp_heartrate_unit;

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Get the value of transaction_guid
         */ 
        public function getTransaction_guid()
        {
                return $this->transaction_guid;
        }

        /**
         * Set the value of transaction_guid
         *
         * @return  self
         */ 
        public function setTransaction_guid($transaction_guid)
        {
                $this->transaction_guid = $transaction_guid;

                return $this;
        }

        /**
         * Get the value of transaction_timestamp
         */ 
        public function getTransaction_timestamp()
        {
                return $this->transaction_timestamp;
        }

        /**
         * Set the value of transaction_timestamp
         *
         * @return  self
         */ 
        public function setTransaction_timestamp($transaction_timestamp)
        {
                $this->transaction_timestamp = $transaction_timestamp;

                return $this;
        }

        /**
         * Get the value of upload_id
         */ 
        public function getUpload_id()
        {
                return $this->upload_id;
        }

        /**
         * Set the value of upload_id
         *
         * @return  self
         */ 
        public function setUpload_id($upload_id)
        {
                $this->upload_id = $upload_id;

                return $this;
        }

        /**
         * Get the value of device_name
         */ 
        public function getDevice_name()
        {
                return $this->device_name;
        }

        /**
         * Set the value of device_name
         *
         * @return  self
         */ 
        public function setDevice_name($device_name)
        {
                $this->device_name = $device_name;

                return $this;
        }

        /**
         * Get the value of serial_number
         */ 
        public function getSerial_number()
        {
                return $this->serial_number;
        }

        /**
         * Set the value of serial_number
         *
         * @return  self
         */ 
        public function setSerial_number($serial_number)
        {
                $this->serial_number = $serial_number;

                return $this;
        }

        /**
         * Get the value of manufacturer_number
         */ 
        public function getManufacturer_number()
        {
                return $this->manufacturer_number;
        }

        /**
         * Set the value of manufacturer_number
         *
         * @return  self
         */ 
        public function setManufacturer_number($manufacturer_number)
        {
                $this->manufacturer_number = $manufacturer_number;

                return $this;
        }

        /**
         * Get the value of model_number
         */ 
        public function getModel_number()
        {
                return $this->model_number;
        }

        /**
         * Set the value of model_number
         *
         * @return  self
         */ 
        public function setModel_number($model_number)
        {
                $this->model_number = $model_number;

                return $this;
        }

        /**
         * Get the value of battery_level
         */ 
        public function getBattery_level()
        {
                return $this->battery_level;
        }

        /**
         * Set the value of battery_level
         *
         * @return  self
         */ 
        public function setBattery_level($battery_level)
        {
                $this->battery_level = $battery_level;

                return $this;
        }

        /**
         * Get the value of user_number
         */ 
        public function getUser_number()
        {
                return $this->user_number;
        }

        /**
         * Set the value of user_number
         *
         * @return  self
         */ 
        public function setUser_number($user_number)
        {
                $this->user_number = $user_number;

                return $this;
        }

        /**
         * Get the value of transmission_timestamp
         */ 
        public function getTransmission_timestamp()
        {
                return $this->transmission_timestamp;
        }

        /**
         * Set the value of transmission_timestamp
         *
         * @return  self
         */ 
        public function setTransmission_timestamp($transmission_timestamp)
        {
                $this->transmission_timestamp = $transmission_timestamp;

                return $this;
        }

        /**
         * Get the value of reading_timestamp
         */ 
        public function getReading_timestamp()
        {
                return $this->reading_timestamp;
        }

        /**
         * Set the value of reading_timestamp
         *
         * @return  self
         */ 
        public function setReading_timestamp($reading_timestamp)
        {
                $this->reading_timestamp = $reading_timestamp;

                return $this;
        }

        /**
         * Get the value of reading_source
         */ 
        public function getReading_source()
        {
                return $this->reading_source;
        }

        /**
         * Set the value of reading_source
         *
         * @return  self
         */ 
        public function setReading_source($reading_source)
        {
                $this->reading_source = $reading_source;

                return $this;
        }

        /**
         * Get the value of bp_systolic_value
         */ 
        public function getBp_systolic_value()
        {
                return $this->bp_systolic_value;
        }

        /**
         * Set the value of bp_systolic_value
         *
         * @return  self
         */ 
        public function setBp_systolic_value($bp_systolic_value)
        {
                $this->bp_systolic_value = $bp_systolic_value;

                return $this;
        }

        /**
         * Get the value of bp_systolic_unit
         */ 
        public function getBp_systolic_unit()
        {
                return $this->bp_systolic_unit;
        }

        /**
         * Set the value of bp_systolic_unit
         *
         * @return  self
         */ 
        public function setBp_systolic_unit($bp_systolic_unit)
        {
                $this->bp_systolic_unit = $bp_systolic_unit;

                return $this;
        }

        /**
         * Get the value of bp_diastolic_value
         */ 
        public function getBp_diastolic_value()
        {
                return $this->bp_diastolic_value;
        }

        /**
         * Set the value of bp_diastolic_value
         *
         * @return  self
         */ 
        public function setBp_diastolic_value($bp_diastolic_value)
        {
                $this->bp_diastolic_value = $bp_diastolic_value;

                return $this;
        }

        /**
         * Get the value of bp_diastolic_unit
         */ 
        public function getBp_diastolic_unit()
        {
                return $this->bp_diastolic_unit;
        }

        /**
         * Set the value of bp_diastolic_unit
         *
         * @return  self
         */ 
        public function setBp_diastolic_unit($bp_diastolic_unit)
        {
                $this->bp_diastolic_unit = $bp_diastolic_unit;

                return $this;
        }

        /**
         * Get the value of bp_heartrate_value
         */ 
        public function getBp_heartrate_value()
        {
                return $this->bp_heartrate_value;
        }

        /**
         * Set the value of bp_heartrate_value
         *
         * @return  self
         */ 
        public function setBp_heartrate_value($bp_heartrate_value)
        {
                $this->bp_heartrate_value = $bp_heartrate_value;

                return $this;
        }

        /**
         * Get the value of bp_heartrate_unit
         */ 
        public function getBp_heartrate_unit()
        {
                return $this->bp_heartrate_unit;
        }

        /**
         * Set the value of bp_heartrate_unit
         *
         * @return  self
         */ 
        public function setBp_heartrate_unit($bp_heartrate_unit)
        {
                $this->bp_heartrate_unit = $bp_heartrate_unit;

                return $this;
        }
} 