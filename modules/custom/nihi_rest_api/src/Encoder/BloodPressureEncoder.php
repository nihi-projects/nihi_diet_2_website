<?php

namespace Drupal\nihi_rest_api\Encoder;

use \Symfony\Component\Serializer\Encoder\XmlEncoder;
use \Symfony\Component\Serializer\Encoder\EncoderInterface;
use \Symfony\Component\Serializer\Encoder\DecoderInterface;
use \Symfony\Component\Serializer\SerializerAwareInterface;
use \Symfony\Component\Serializer\SerializerAwareTrait;

class BloodPressureEncoder implements EncoderInterface, DecoderInterface, SerializerAwareInterface {

  use SerializerAwareTrait;
  
  /**
   * {@inheritdoc}
   */
  static protected $format = ['blood_pressure_xml'];

  /**
   * {@inheritdoc}
   */
  public function getBaseEncoder() {
    $base_encoder = new XmlEncoder();
    $base_encoder->setSerializer($this->serializer);
    $base_encoder->setRootNodeName('CmTransaction');
    return $base_encoder;
  }

  public function encode($data, $format, array $context = [])
  {
    $encoder = new XmlEncoder();
    $encoder = $this->getBaseEncoder();
    $xml = $encoder->encode($data, $format, $context);
    $result = urlencode($xml);
    
    return $result;
  }

  public function supportsEncoding($format)
  {
    return "blood_pressure_xml" === $format;
  }

  public function decode($data, $format, array $context = [])
  {
    \Drupal::logger('system')->info("Decode: ".$data);
    $decodeData = urldecode($data);
    $decoder = $this->getBaseEncoder();
    $result = $decoder->decode($decodeData, $format, $context);
    return $result;
  }

  public function supportsDecoding($format)
  {
    return "xml" === $format;
  }
}