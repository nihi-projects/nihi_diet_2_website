<?php

namespace Drupal\nihi_rest_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Component\Plugin\Exception\PluginException;
use \DateTime;
use \DateTimeZone;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use Drupal\nihi_rest_api\BloodPressure;

/**
 * Provides a Nihi Resource
 *
 * @RestResource(
 *   id = "nihi_blood_pressure",
 *   label = @Translation("Nihi Blood Pressure"),
 *   uri_paths = {
 *     "canonical" = "/nihi_rest_api/blood_pressure",
 * 	   "create" = "/nihi_rest_api/blood_pressure"
 *   }
 * )
 */
class NihiBloodPressureResource extends ResourceBase {
	
	/**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new NihiBloodPressureResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('nihi_rest_api'),
      $container->get('current_user')
    );
  }

	public function post(array $data) {
    $obj = json_decode(json_encode($data), true);
    $response = $this->saveUploadData($obj);
        
    return new ResourceResponse($response);
  }

  private function saveUploadData($data) {
    
    if ($data) {
      $uploadArray = $this->getMultipleNode($data["TransBody"]["Request"]["UploadReading"]);
      
      $responses = array();
      foreach ($uploadArray as $upload) {
        try {
          if ($data["TransHeader"]["RecipientId"] != "uoauckland" || $data["TransHeader"]["PassPhrase"] != "nde2oh!2dDG") {
            throw new AccessDeniedHttpException('Invalid Recipient ID and pass phrase');
          }

          $bp = new BloodPressure();
          $bp->setTransaction_guid($data["TransHeader"]["TransactionGUID"]);
          $bp->setTransaction_timestamp(strtotime($data["TransHeader"]["TransactionTimeStamp"]));
          $bp->setUpload_id($upload["@Id"]);
          $bp->setDevice_name($upload["Device"]["DeviceName"]);
          $bp->setSerial_number($upload["Device"]["SerialNumber"]);
          $bp->setManufacturer_number($upload["Device"]["ManufacturerNumber"]);
          $bp->setModel_number($upload["Device"]["ModelNumber"]);
          $bp->setBattery_level($upload["Device"]["BatteryLevel"]);
          $bp->setUser_number($upload["UserNumber"]);
          $bp->setTransmission_timestamp(strtotime($upload["TransmissionTimeStamp"]));
          $bp->setReading_timestamp(strtotime($upload["ReadingTimeStamp"]));
          $bp->setReading_source($upload["ReadingSource"]);

          $readingArray = $this->getMultipleNode($upload["Reading"]);
          foreach ($readingArray as $reading) {
            if ($reading["ReadingType"] === "BP Systolic") {
              $bp->setBp_systolic_value($reading["ReadingValue"]);
              $bp->setBp_systolic_unit($reading["Unit"]);
            } else if ($reading["ReadingType"] === "BP Diastolic") {
              $bp->setBp_diastolic_value($reading["ReadingValue"]);
              $bp->setBp_diastolic_unit($reading["Unit"]);
            } else if ($reading["ReadingType"] === "BP Heartrate") {
              $bp->setBp_heartrate_value($reading["ReadingValue"]);
              $bp->setBp_heartrate_unit($reading["Unit"]);
            }
          }

          $this->saveBloodPressureData($bp);

          $this->updateAvgData($bp);

          $responses[] = $this->setResponse($upload["@Id"], TRUE);
        } catch (\Exception $e) {
          \Drupal::logger('nihi_rest_api')->error("Failure upload data: ".$upload["@Id"].": ".$e->getMessage());
          $responses[] = $this->setResponse($upload["@Id"], FALSE, $e->getMessage());
        }
      }

      unset($data["TransBody"]["Request"]);
      foreach ($responses as $response) {
        $temp["@Id"] = $response->id;
        $temp["Status"] = $response->status;
        $temp["Description"] = $response->desc;
        $data["TransBody"]["Response"]["Result"][] = $temp;
      }
    }

    return $data;
  }

  private function getMultipleNode($input) {
    $output = array();
    if ($input[0]) {
      // it has multiple input
      $output = $input;
    } else {
      // it has only one node, convert to array
      $output[] = $input;
    }
    return $output;
  }

  private function setResponse($id, $isSuccess, $message = NULL) {
    $result = new \stdClass();
    $result->id = $id;
    if ($isSuccess) {
      $result->status = "Success";
      $result->desc = "Successful Reading Upload";
    } else {
      $result->status = "Failure";
      $result->desc = $message;
    }
    
    return $result;
  }

  private function saveBloodPressureData($bp) {
    $database = \Drupal::database();
    $fields = array(
      'transaction_guid' => $bp->getTransaction_guid(),
      'transaction_timestamp' => $bp->getTransaction_timestamp(),
      'upload_id' => $bp->getUpload_id(),
      'device_name' => $bp->getDevice_name(),
      'serial_number' => $bp->getSerial_number(),
      'manufacturer_number' => $bp->getManufacturer_number(),
      'model_number' => $bp->getModel_number(),
      'battery_level' => $bp->getBattery_level(),
      'user_number' => $bp->getUser_number(),
      'transmission_timestamp' => $bp->getTransmission_timestamp(),
      'reading_timestamp' => $bp->getReading_timestamp(),
      'reading_source' => $bp->getReading_source(),
      'bp_systolic_value' => $bp->getBp_systolic_value(),
      'bp_systolic_unit' => $bp->getBp_systolic_unit(),
      'bp_diastolic_value' => $bp->getBp_diastolic_value(),
      'bp_diastolic_unit' => $bp->getBp_diastolic_unit(),
      'bp_heartrate_value' => $bp->getBp_heartrate_value(),
      'bp_heartrate_unit' => $bp->getBp_heartrate_unit()
    );

    $database->insert('blood_pressure')
    ->fields($fields)
    ->execute();
  }

  private function updateAvgData($bp) {
    $readingTime = new DateTime("@".$bp->getReading_timestamp());
    $readingTime->setTimezone(new DateTimeZone('Pacific/Auckland'));   
    $hour = (int) $readingTime->format('G');

    $readingDate = $readingTime->format('Y-m-d');
    $from = 0;
    $to = 0;

    $session = "";
    if ($hour < 12) {
      $session = "morning";
      // 00:00 - 12:00
      $from = $readingTime->setTime(0, 0)->getTimeStamp();
      $to = $readingTime->setTime(12, 0)->getTimeStamp();
    } else {
      $session = "evening";
      // 12:00 - 24:00
      $from = $readingTime->setTime(12, 0)->getTimeStamp();
      $to = $readingTime->setTime(24, 0)->getTimeStamp();
    }

    $connection = \Drupal::database();

    // Original SQL query for average values.
    /*
    $result = $connection->query(
      "SELECT AVG(bp_systolic_value) avg_bp_systolic, AVG(bp_diastolic_value) avg_bp_diastolic, AVG(bp_heartrate_value) avg_bp_heartrate, COUNT(1) cnt 
       FROM {blood_pressure} 
       WHERE user_number = 1 AND reading_timestamp >= :startTime AND reading_timestamp < :endTime AND serial_number = :serial_number
       GROUP BY serial_number;",
       array(':startTime' => $from, ':endTime' => $to, ':serial_number' => $bp->getSerial_number()));
    */

    // Modified SQL query which only calculates a person's averages from two most recent measures.
    $result = $connection->query(
      "SELECT 
          AVG(last_two_measures.bp_systolic_value) avg_bp_systolic, 
          AVG(last_two_measures.bp_diastolic_value) avg_bp_diastolic, 
          AVG(last_two_measures.bp_heartrate_value) avg_bp_heartrate, 
          COUNT(1) cnt 
        FROM
        (
          SELECT bp_systolic_value, bp_diastolic_value, bp_heartrate_value
          FROM {blood_pressure}
          WHERE user_number = 1  
            AND reading_timestamp >= :startTime 
            AND reading_timestamp < :endTime 
            AND serial_number = :serial_number	
          ORDER BY reading_timestamp DESC
          LIMIT 2
        ) last_two_measures;",
      array(':startTime' => $from, ':endTime' => $to, ':serial_number' => $bp->getSerial_number()));

    $record = $result->fetch(); 

    if ($record) {
      // Find existing avgerage value
      $nids = \Drupal::entityQuery('node')
        ->condition('type', 'blood_pressure_average')
        ->condition('field_serial_number', $bp->getSerial_number())
        ->condition('field_measurement_date', $readingDate)
        ->condition('field_measurement_session', $session)
        ->execute();

      if ($nids && count($nids) > 0) {
        // Get first one and should only one
        $node = Node::load(reset($nids));
      } else {
        // New measurement for this date and session
        $node = Node::create(['type' => 'blood_pressure_average']);
        $node->set('title', $bp->getSerial_number()." ".$readingDate." ".$session);
        $node->set('field_serial_number', $bp->getSerial_number());
        $node->set('field_measurement_date', $readingDate);
        $node->set('field_measurement_session', $session);
        $node->enforceIsNew();
      }
      $node->set('field_avg_bp_systolic', $record->avg_bp_systolic);
      $node->set('field_avg_bp_diastolic', $record->avg_bp_diastolic);
      $node->set('field_avg_heart_rate', $record->avg_bp_heartrate);
      $node->set('field_measurement_count', $record->cnt);
      $node->save();
    }
  }
}