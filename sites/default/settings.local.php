<?php

$config_directories['sync'] = 'config/sync';

$databases['default']['default'] = array (
  'database' => 'stage_diet',
  'username' => 'diet',
  'password' => '$*L3RXkH',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['trusted_host_patterns'] = array(
  '^localhost$',
  '^.+\.auckland\.ac\.nz$'
);

$settings['file_private_path'] = '/var/www/private';

$settings['http_client_config']['proxy']['http'] = 'http://squid.auckland.ac.nz:3128/';
$settings['http_client_config']['proxy']['https'] = 'http://squid.auckland.ac.nz:3128/'; 

